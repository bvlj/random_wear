package it.jrizzoli.random.fragments

import android.graphics.drawable.AnimatedVectorDrawable
import it.jrizzoli.random.R

class CoinFragment : RandomFragment() {

    override val maxRandom: Int
        get() = 1

    override fun setupUI() {
        mIcon.setImageResource(R.drawable.avd_coin)
        mText.text = getString(R.string.coin_flip)
    }

    override fun triggerAction() {
        (mIcon.drawable as AnimatedVectorDrawable).start()
        val result = randomResult
        mText.text = getString(if (result == 1) R.string.coin_true else R.string.coin_false)
    }
}
