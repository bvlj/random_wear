package it.jrizzoli.random.fragments

import android.view.View
import it.jrizzoli.random.R

class DiceFragment : RandomFragment() {

    override val maxRandom: Int
        get() = 6

    override fun setupUI() {
        mIcon.setImageResource(R.drawable.ic_dice_1)
        mText.visibility = View.GONE
    }

    override fun triggerAction() {
        mIcon.animate()
                .rotation(360f)
                .withEndAction { rollDice() }
                .start()
    }

    private fun rollDice() {
        val result = randomResult
        val icon = when (result) {
            0 -> R.drawable.ic_dice_1
            1 -> R.drawable.ic_dice_2
            2 -> R.drawable.ic_dice_3
            3 -> R.drawable.ic_dice_4
            4 -> R.drawable.ic_dice_5
            5 -> R.drawable.ic_dice_6
            else -> 0
        }

        if (icon != 0) {
            mIcon.setImageResource(icon)
        }

        mIcon.rotation = 0f
    }
}
