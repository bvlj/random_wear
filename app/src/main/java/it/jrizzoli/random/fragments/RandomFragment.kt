package it.jrizzoli.random.fragments

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import it.jrizzoli.random.R
import java.security.SecureRandom

abstract class RandomFragment : Fragment() {
    protected lateinit var mIcon: ImageView
    protected lateinit var mText: TextView

    protected val randomResult: Int
        get() = SecureRandom().nextInt(maxRandom + 1)

    protected abstract val maxRandom: Int

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?,
                              savedInstance: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_random, parent, false)

        val rootLayout = view.findViewById<RelativeLayout>(R.id.random_layout)
        mIcon = view.findViewById(R.id.random_image)
        mText = view.findViewById(R.id.random_text)

        rootLayout.setOnClickListener { _ -> triggerAction() }

        setupUI()
        return view
    }

    protected abstract fun setupUI()
    abstract fun triggerAction()
}