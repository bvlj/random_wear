package it.jrizzoli.random

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.support.wear.widget.drawer.WearableActionDrawerView
import android.support.wear.widget.drawer.WearableDrawerLayout
import android.support.wear.widget.drawer.WearableNavigationDrawerView
import android.support.wearable.activity.WearableActivity
import android.view.ViewTreeObserver
import it.jrizzoli.random.fragments.CoinFragment
import it.jrizzoli.random.fragments.DiceFragment

class MainActivity : WearableActivity(), WearableNavigationDrawerView.OnItemSelectedListener,
        SensorEventListener{

    private lateinit var mDrawerLayout: WearableDrawerLayout
    private lateinit var mNavDrawer: WearableNavigationDrawerView
    private lateinit var mActionDrawer: WearableActionDrawerView
    private lateinit var mVibrator: Vibrator

    private val mDice = DiceFragment()
    private val mCoin = CoinFragment()

    private var mPosition = 0
    private var mLastTrigger = System.currentTimeMillis()
    private var mLastPosition = Array(3, { 0f })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setAmbientEnabled()

        mVibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        mDrawerLayout = findViewById(R.id.drawer_layout)
        mNavDrawer = findViewById(R.id.nav_drawer)
        mActionDrawer = findViewById(R.id.action_drawer)
        mNavDrawer.setAdapter(NavAdapter(this))
        mNavDrawer.addOnItemSelectedListener(this)

        val observer = mDrawerLayout.viewTreeObserver
        observer.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                mDrawerLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                mNavDrawer.controller.peekDrawer()
            }
        })

        fragmentManager.beginTransaction().replace(R.id.content_frame, mCoin).commit()

        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onItemSelected(pos: Int) {
        setFragment(pos)
        mActionDrawer.controller.closeDrawer()
    }

    override fun onSensorChanged(p0: SensorEvent?) {
        val time = System.currentTimeMillis()
        val diffTime = time - mLastTrigger
        if (p0 == null || diffTime < 1000) {
            return
        }

        mLastTrigger = time

        val x = p0.values[0]
        val y = p0.values[1]
        val z = p0.values[2]

        val speed = Math.abs(x + y + z - mLastPosition[0] - mLastPosition[1] - mLastPosition[2]) /
                diffTime * 10000

        if (speed < 200) {
            return
        }

        mLastPosition = arrayOf(x, y, z)

        onShakeAction()
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) = Unit

    private fun setFragment(position: Int) {
        mPosition = position
        when (position) {
            0 -> fragmentManager.beginTransaction().replace(R.id.content_frame, mCoin).commit()
            1 -> fragmentManager.beginTransaction().replace(R.id.content_frame, mDice).commit()
        }
    }

    private fun onShakeAction() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mVibrator.vibrate(VibrationEffect.createOneShot(500,
                    VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            @Suppress("DEPRECATION")
            mVibrator.vibrate(500)
        }

        when (mPosition) {
            0 -> mCoin.triggerAction()
            1 -> mDice.triggerAction()
        }
    }
}
