package it.jrizzoli.random

import android.content.Context
import android.support.wear.widget.drawer.WearableNavigationDrawerView

internal class NavAdapter(private val mContext: Context) :
        WearableNavigationDrawerView.WearableNavigationDrawerAdapter() {

    override fun getCount() = 2

    override fun getItemText(position: Int) =
            when (position) {
                0 -> mContext.getString(R.string.menu_coin)
                1 -> mContext.getString(R.string.menu_dice)
                else -> null
            }

    override fun getItemDrawable(position: Int) =
            when (position) {
                0 -> mContext.getDrawable(R.drawable.ic_menu_coin)
                1 -> mContext.getDrawable(R.drawable.ic_menu_dice)
                else -> null
            }
}
